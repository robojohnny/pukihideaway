'use strict';

// store all the contact details on click of submit
function store() {
	var inputEmail = document.getElementById('email').value;
	var inputPhone = document.getElementById('phone').value;
	var inputFName = document.getElementById('fname').value;
	var inputLName = document.getElementById('lname').value;
	var inputMessage = document.getElementById('message').value;
	//console.log('"',inputMessage,'"')
	if (inputMessage === '') {
		alert('enter your message');
		return;
	}
	var contact = {
		'email' : inputEmail,
		'phone' : inputPhone,
		'fname' : inputFName,
		'lname' : inputLName,
		'message' : inputMessage
	}

	var contacts;

	var localStorageValue = localStorage.getItem(LOCALSTORAGEKEY + '-contacts');
	//debugger;
	if (localStorageValue === null) {
		contacts = [];
	} else {
		contacts = JSON.parse(localStorageValue);
	}
	contacts.push(contact);

	// store contacts array to local storage
	localStorage.setItem(LOCALSTORAGEKEY + '-contacts', JSON.stringify(contacts));
	alert('Thank you for contacting us');
}
