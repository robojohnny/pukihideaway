'use strict';

var gUsers;

function initiliaseContacts() {
	var localStorageValue = localStorage.getItem(LOCALSTORAGEKEY + '-contacts');
	if (localStorageValue) {
		var contacts = JSON.parse(localStorageValue);
		return contacts;
	}
}

function printContacts(contacts) {

	var elTable = document.querySelector('.usersList');

	var strHTML = '<table border="1"><tbody>';
	strHTML += '<tr>'
	strHTML += '<th> ' + 'First Name' + ' </th>'
	strHTML += '<th> ' + 'Last Name' + ' </th>'
	strHTML += '<th> ' + 'Email' + ' </th>'
	strHTML += '<th> ' + 'Phone' + ' </th>'
	strHTML += '<th> ' + 'Message' + ' </th>'
	strHTML += '<th></th>'
	strHTML += '</tr>'

	contacts.forEach(function (row,index) {
		strHTML += '<tr>';
		strHTML += '<td> ' + row.fname + ' </td>'
		strHTML += '<td> ' + row.lname + ' </td>'
		strHTML += '<td> ' + row.email + ' </td>'
		strHTML += '<td> ' + row.phone + ' </td>'
		strHTML += '<td><pre>' + row.message + '</pre></td>'
		strHTML += '<td><button onclick="deleteContact('+index+')">delete</button></td>'
		strHTML += '</tr>'
	})
	strHTML += '</tbody></table>';

	elTable.innerHTML = strHTML;
}

function deleteContact(index){
	var contacts=initiliaseContacts();
	contacts.splice(index,1);
	localStorage.setItem(LOCALSTORAGEKEY + '-contacts',JSON.stringify(contacts));
	printContacts(contacts);
}
function initAdminPage() {
	var contacts = initiliaseContacts();
	if (contacts) {
		printContacts(contacts);
	} else {
		alert('no one contacted us!');
	}
}
